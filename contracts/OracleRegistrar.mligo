// Right-combed parameter type to SetChildRecord.
type set_child_record_param = [@layout:comb] {
    // The UTF-8 encoded label.
    label: bytes;

    // The UTF-8 encoded parent domain.
    parent: bytes;

    // The optional address the record resolves to.
    address: address option;

    // The owner of the record allowed to make changes.
    owner: address;

    // A map of any additional data clients wish to store with the domain.
    data: (string, bytes) map;

    // The expiry of this record. Only applicable to second-level domains as all higher-level domains share the expiry of their ancestor 2LD.
    expiry: timestamp option;
}

type storage = [@layout:comb] {
    set_child_record: address;
    sign_keys: key set;
    admin: address;
    max_timestamp_age: nat;
    claim_price: tez;
    treasury: address;
}

type claim_payload = [@layout:comb] {
    label: bytes;
    tld: bytes;
    owner: address;
    timestamp: timestamp;
}

type claim_param = [@layout:comb] {
    payload: claim_payload;
    signature: signature;
}

type return = (operation list) * storage

[@inline]
let get_treasury (store: storage) : unit contract =
    (Tezos.get_contract_with_error store.treasury "INVALID_TREASURY" : unit contract)

[@inline]
let require_correct_amount (store: storage) : tez = 
    if Tezos.amount < store.claim_price then
        (failwith "AMOUNT_TOO_LOW" : tez)
    else if Tezos.amount > store.claim_price then
        (failwith "AMOUNT_TOO_HIGH" : tez)
    else Tezos.amount

let claim (param, store: claim_param * storage): (operation list) =
    // validate the timestamp
    let _assert_valid_timestamp = if Tezos.now < param.payload.timestamp + int(store.max_timestamp_age) then () else (failwith "TIMESTAMP_TOO_OLD" : unit) in

    // validate the signature
    let payload_bytes = Bytes.pack param.payload in
    let validate_signature = (fun (a, key : bool * key) -> a || Crypto.check key param.signature payload_bytes) in
    let _assert_valid_signature = if Set.fold validate_signature store.sign_keys False then () else (failwith "INVALID_SIGNATURE" : unit) in
    let correct_amount = require_correct_amount(store) in

    // send to SetChildRecord
    let child_record_param : set_child_record_param = {
        label = param.payload.label;
        parent = param.payload.tld;
        address = (None : address option);
        owner = param.payload.owner;
        data = (Map.empty : (string, bytes) map);
        expiry = (None : timestamp option);
    } in
    let set_child_record_contract = (Tezos.get_contract_with_error store.set_child_record "INVALID_SET_CHILD_RECORD" : set_child_record_param contract) in
    let set_child_record_transaction = Tezos.transaction child_record_param 0mutez set_child_record_contract in
    let send_proceeds = Tezos.transaction () correct_amount (get_treasury store) in

    [set_child_record_transaction; send_proceeds]

let admin_update (param, store: storage * storage): return =
    // validate the admin address
    let _assert_valid_admin = if store.admin = Tezos.sender then () else (failwith "NOT_AUTHORIZED" : unit) in

    ([] : operation list), param

type parameter =
    Claim of claim_param
    | Admin_update of storage

let main (action, store : parameter * storage) : return =
    match action with
        | Claim p -> claim (p, store), store
        | Admin_update p -> admin_update (p, store)
