const fs = require('fs');
const path = require('path');
const file = path.resolve(process.cwd(), '.taq/config.json');

function updateRpcUrl() {
    if (!process.env.RPC_URL) {
        return;
    }

    const configContent = require(file);
    // console.log(configContent);
    configContent.sandbox.local.rpcUrl = process.env.RPC_URL;

    fs.writeFileSync(file, JSON.stringify(configContent, null, 2));
}

updateRpcUrl();
