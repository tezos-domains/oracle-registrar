import { TezosToolkit } from '@taquito/taquito';
import { strict as assert } from 'assert';

export async function originateSetChildRecord(tezos: TezosToolkit): Promise<string> {
    const originationOp = await tezos.contract.originate({
        code: `{ parameter
                (or (unit %dummy)
                    (pair %set_child_record
                       (bytes %label)
                       (bytes %parent)
                       (option %address address)
                       (address %owner)
                       (map %data string bytes)
                       (option %expiry timestamp))) ;
              storage
                (option
                   (pair (bytes %label)
                         (bytes %parent)
                         (option %address address)
                         (address %owner)
                         (map %data string bytes)
                         (option %expiry timestamp))) ;
              code { CAR ;
                     IF_LEFT
                       { DROP ; PUSH string "unexpected" ; FAILWITH }
                       { SOME ; NIL operation ; PAIR } } }`,
        storage: null,
    });

    await originationOp.confirmation();

    assert(originationOp.contractAddress, 'setChildRecord contract mock failed to be created');

    return originationOp.contractAddress!;
}
/**

// Right-combed parameter type to SetChildRecord.
type set_child_record_param = [@layout:comb] {
    // The UTF-8 encoded label.
    label: bytes;

    // The UTF-8 encoded parent domain.
    parent: bytes;

    // The optional address the record resolves to.
    address: address option;

    // The owner of the record allowed to make changes.
    owner: address;

    // A map of any additional data clients wish to store with the domain.
    data: (string, bytes) map;

    // The expiry of this record. Only applicable to second-level domains as all higher-level domains share the expiry of their ancestor 2LD.
    expiry: timestamp option;
}

type proxy_parameter =
    Set_child_record of set_child_record_param
    | Dummy of unit

// Creates or overwrites an existing domain record. The current sender must be the owner of the parent record.
let main (action, store : proxy_parameter * (set_child_record_param option)) : (operation list * (set_child_record_param option)) =    
    match action with
        Set_child_record p -> (([] : operation list), Some p)
        | Dummy p -> (failwith "unexpected" : (operation list * (set_child_record_param option)))

 */
