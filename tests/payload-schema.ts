import { MichelsonType, MichelsonTypePair } from '@taquito/michel-codec';

export const PayloadSchema: MichelsonTypePair<MichelsonType[]> = {
    prim: 'pair',
    args: [
        {
            prim: 'bytes',
            annots: ['%label'],
        },
        {
            prim: 'bytes',
            annots: ['%tld'],
        },
        {
            prim: 'address',
            annots: ['%owner'],
        },
        {
            prim: 'timestamp',
            annots: ['%timestamp'],
        },
    ],
};
