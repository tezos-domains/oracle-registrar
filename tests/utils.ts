export function encodeString(str: string): string {
    var result = '';
    var encoded = new TextEncoder().encode(str);
    for (let i = 0; i < encoded.length; i++) {
        let hexchar = encoded[i].toString(16);
        result += hexchar.length == 2 ? hexchar : '0' + hexchar;
    }
    return result;
}
