import { packDataBytes } from '@taquito/michel-codec';
import { MichelsonMap, Schema } from '@taquito/michelson-encoder';
import { InMemorySigner } from '@taquito/signer';
import { DefaultContractType, OriginationOperation, Signer, TezosToolkit } from '@taquito/taquito';
import BigNumber from 'bignumber.js';
import { PayloadSchema } from './payload-schema';
import { originateSetChildRecord } from './setChildRecord.contract';
import { encodeString } from './utils';

type ContractStorage = { max_timestamp_age: BigNumber; sign_keys: string[]; admin: string };

const config = require('../.taq/config.json');

describe('JavaScript tests for OracleRegistrar contract', () => {
    let Tezos: TezosToolkit;

    const TreasuryAddress = 'tz1VBLpuDKMoJuHRLZ4HrCgRuiLpEr7zZx2E';
    const ClaimPrice = 1_500_000;
    const invalidSignature = 'edsigtcyKS5n6oKbgEeMr7XoyjB2yFa7wzcYjc6dcnCMWJFGquRRWRx5QNJWzZCfQcpVCJfLjLj77rkM1BUV8h4cfMcQX2ctHZy';
    const { alice, bob, jane } = config.sandbox.local.accounts;
    const { rpcUrl } = config.sandbox.local;

    jest.setTimeout(50000);

    beforeAll(async () => {
        Tezos = new TezosToolkit(rpcUrl);
        signWithWallet(alice);
    });

    describe('when set_child_record contract mock exists', () => {
        let setChildRecordContractAddress: string;
        let contractAddress: string;
        let originationOp: OriginationOperation<DefaultContractType>;

        beforeAll(async () => {
            setChildRecordContractAddress = await originateSetChildRecord(Tezos);
            originationOp = await originateContract({ setChildRecordContract: setChildRecordContractAddress });

            contractAddress = originationOp.contractAddress!;
        });

        describe('claim()', () => {
            test('Should fail with TIMESTAMP_TOO_OLD', async () => {
                const contract = await getOracleContract(contractAddress);
                let err: Error | null = null;

                try {
                    await contract.methods
                        .claim(encodeString('test'), encodeString('com'), alice.keys.publicKeyHash, new Date(2000, 0, 1), invalidSignature)
                        .send();
                } catch (e) {
                    err = e;
                }

                expect(err?.message).toEqual('TIMESTAMP_TOO_OLD');
            });

            test('Should fail with INVALID_SIGNATURE', async () => {
                const contract = await getOracleContract(contractAddress);
                let err: Error | null = null;

                try {
                    await contract.methods.claim(encodeString('test'), encodeString('com'), alice.keys.publicKeyHash, new Date(), invalidSignature).send();
                } catch (e) {
                    err = e;
                }

                expect(err?.message).toEqual('INVALID_SIGNATURE');
            });
        });

        describe('with multiple public keys', () => {
            let setChildRecordAddress: string = '';

            beforeAll(async () => {
                setChildRecordAddress = await originateSetChildRecord(Tezos);
                const op = await originateContract({
                    setChildRecordContract: setChildRecordAddress,
                    signatureKeys: [bob.keys.encryptedKey, alice.keys.encryptedKey],
                });
                contractAddress = op.contractAddress!;
            });

            describe('claim()', () => {
                const now = new Date();
                const data = {
                    label: encodeString('test'),
                    tld: encodeString('com'),
                    owner: bob.keys.publicKeyHash,
                    timestamp: now.toISOString(),
                };

                [bob, alice].forEach(wallet => {
                    test(`should succeed with secret key from: '${wallet.keys.alias}'`, async () => {
                        await expect(callClaim(contractAddress, secretKey(wallet), data)).resolves.toBeGreaterThan(0);
                    });
                });

                test(`should fail when called with unknown keys`, async () => {
                    await expect(callClaim(contractAddress, secretKey(jane), data)).rejects.toThrow('INVALID_SIGNATURE');
                });

                test(`should call set_child_record with correct params`, async () => {
                    await callClaim(contractAddress, secretKey(bob), data);

                    const setChildRecordStorage = await (await Tezos.contract.at(setChildRecordAddress)).storage<any>();
                    expect(setChildRecordStorage).toMatchObject({
                        label: data.label,
                        parent: data.tld,
                        owner: data.owner,
                        expiry: null,
                        address: null,
                        data: expect.any(MichelsonMap),
                    });
                });
            });

            describe('when claiming', () => {
                const now = new Date();
                const data = {
                    label: encodeString('test'),
                    tld: encodeString('com'),
                    owner: bob.keys.publicKeyHash,
                    timestamp: now.toISOString(),
                };

                test(`should transfer money to treasury`, async () => {
                    const initialBallance = await getTreasuryBalance();

                    await callClaim(contractAddress, secretKey(bob), data);

                    const currentBallance = await getTreasuryBalance();

                    expect(currentBallance - initialBallance).toBe(ClaimPrice);
                });

                test(`should fail with AMOUNT_TOO_LOW`, async () => {
                    await expect(callClaim(contractAddress, secretKey(bob), data, 1)).rejects.toThrow('AMOUNT_TOO_LOW');
                });

                test(`should fail with AMOUNT_TOO_HIGH`, async () => {
                    await expect(callClaim(contractAddress, secretKey(bob), data, 2)).rejects.toThrow('AMOUNT_TOO_HIGH');
                });
            });
        });

        describe('admin_update() call from admin user', () => {
            test('Should update storage successfully', async () => {
                const contract = await getOracleContract(contractAddress);
                let storage = await contract.storage<ContractStorage>();
                expect(storage.max_timestamp_age.toNumber()).toBe(300);

                await callAdminUpdate(alice, { timeout: 310 });

                storage = await contract.storage<ContractStorage>();

                expect(storage.max_timestamp_age.toNumber()).toBe(310);
            });
        });
        describe('admin_update() call from non admin user', () => {
            beforeEach(() => {
                signWithWallet(bob);
            });
            test('Should fail with NOT_AUTHORIZED', async () => {
                await expect(callAdminUpdate(bob, { timeout: 310 })).rejects.toThrow('NOT_AUTHORIZED');
            });

            afterEach(() => {
                signWithWallet(alice);
            });
        });

        async function callAdminUpdate(wallet: any, data: { timeout: number }) {
            const contract = await getOracleContract(contractAddress);

            const signer = new InMemorySigner(wallet.keys.secretKey.replace(/unencrypted:/, ''));
            Tezos.setSignerProvider(signer);

            const op = await contract.methods
                .admin_update(
                    'KT1WX2SYvNMo1onPRfsgBVctYLvkHLRLduxk%set_child_record',
                    ['edpkurPsQ8eUApnLUJ9ZPDvu98E8VNj4KtJa1aZr16Cr5ow5VHKnz4'],
                    alice.keys.publicKeyHash,
                    data.timeout,
                    ClaimPrice,
                    TreasuryAddress
                )
                .send();

            await op.confirmation();
        }
    });

    describe('when no set_child_record contract exists', () => {
        let contractAddress: string;
        let originationOp: OriginationOperation<DefaultContractType>;
        const now = new Date();
        const data = {
            label: encodeString('test'),
            tld: encodeString('com'),
            owner: bob.keys.publicKeyHash,
            timestamp: now.toISOString(),
        };

        beforeAll(async () => {
            originationOp = await originateContract();
            contractAddress = originationOp.contractAddress!;
        });

        describe('claim()', () => {
            test('Should fail with INVALID_SET_CHILD_RECORD', async () => {
                await expect(callClaim(contractAddress, secretKey(bob), data)).rejects.toThrow('INVALID_SET_CHILD_RECORD');
            });
        });
    });

    async function callClaim(contractAddress: string, secretKey: string, data: { label: string; owner: string; tld: string; timestamp: string }, amount = 1.5) {
        const contract = await getOracleContract(contractAddress);

        const schema = new Schema(PayloadSchema);
        const now = data.timestamp;

        const packed = packDataBytes(schema.Encode(data), PayloadSchema).bytes;

        const signer = await InMemorySigner.fromSecretKey(secretKey);
        const signature = await signer.sign(packed);

        const op = await contract.methods.claim(data.label, data.tld, data.owner, now, signature.sig).send({ amount });

        return await op.confirmation();
    }

    function signWithWallet(wallet: any) {
        const signer = new InMemorySigner(secretKey(wallet));
        Tezos.setSignerProvider(signer);
    }

    async function getTreasuryBalance() {
        return (await Tezos.tz.getBalance(TreasuryAddress)).toNumber();
    }

    async function originateContract(
        options: {
            setChildRecordContract?: string;
            signatureKeys?: string[];
        } = {}
    ): Promise<OriginationOperation<DefaultContractType>> {
        const { readFile } = require('fs/promises');
        const { join } = require('path');
        const contractFile = join(config.artifactsDir, 'OracleRegistrar.tz');
        const setChildRecordContractAddress = options.setChildRecordContract || 'KT1WX2SYvNMo1onPRfsgBVctYLvkHLRLduxk';
        const storage = {
            set_child_record: `${setChildRecordContractAddress}%set_child_record`,
            sign_keys: options.signatureKeys ?? ['edpkurPsQ8eUApnLUJ9ZPDvu98E8VNj4KtJa1aZr16Cr5ow5VHKnz4'],
            admin: alice.keys.publicKeyHash,
            max_timestamp_age: 300,
            claim_price: ClaimPrice,
            treasury: TreasuryAddress,
        };

        const originationOp = await Tezos.contract.originate({
            code: await readFile(contractFile, { encoding: 'utf8' }),
            storage,
        });
        await originationOp.confirmation();
        return originationOp;
    }

    async function getOracleContract(address: string) {
        const contract = await Tezos.contract.at(address);
        const storage = await contract.storage<ContractStorage>();

        expect(storage.admin).toEqual(alice.keys.publicKeyHash);

        return contract;
    }

    function secretKey(wallet: any): string {
        return wallet.keys.secretKey.replace(/unencrypted:/, '');
    }
});
